[cmdletbinding()]
Param (
    [string]$Url,
    [string]$Name,
    [string]$DisplayName,
    [string]$Description,
    [version]$Version,
    [string[]]$TaskList
)

ForEach ($Module in @("Pester","Psake","ScriptAsService")) {
    If (!(Get-Module -ListAvailable $Module)) {
        Find-Module $Module | Install-Module -Force
    }
    Import-Module $Module
}

Push-Location $PSScriptRoot

If ($TaskList.Count -gt 0) {
    Write-Output "Executing Tasks: $TaskList`r`n"
    Invoke-Psake -buildFile .\psake.ps1 -properties $PSBoundParameters -noLogo -taskList $TaskList
} Else {
    Write-Output "Executing Unit Tests Only`r`n"
    Invoke-Psake -buildFile .\psake.ps1 -properties $PSBoundParameters -nologo
}