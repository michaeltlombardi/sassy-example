If ([string]::IsNullOrEmpty((Get-Module -ListAvailable flancy))) {
    git clone https://github.com/toenuff/flancy.git "$home/Documents/WindowsPowerShell/Modules/flancy"
}

Import-Module flancy

New-Flancy -Url "<FLANCY_URL>" -WebSchema @(
    Get '/' { "Whatchu want?" }
    
    Get '/process' {
        Get-Process `
        | Select-Object -Property Name, Id, Path `
        | ConvertTo-Json
    }
    Post '/process' {
        $ProcessName = (new-Object System.IO.StreamReader @($Request.Body, [System.Text.Encoding]::UTF8)).ReadToEnd()
        "You think I'm just going to start $ProcessName on your say so?"
    }

    Get '/process/{name}' {
        Get-Process $parameters.name `
        | ConvertTo-Json -Depth 1
    }

    Get '/health' {
        "You can use this endpoint to execute any of a series of PoshSpec tests against this node: memory, disk, or cpu"
    }

    Get '/health/memory' {
        $Total = Get-CimInstance -Class "cim_physicalmemory" `
        | Measure-Object -Property capacity -Sum `
        | Select-Object -ExpandProperty Sum
        $Free = Get-CimInstance -ClassName win32_OperatingSystem `
        | Select-Object -ExpandProperty FreePhysicalMemory
        $PercentFree = (($Free /1KB) / ($Total / 1MB))
        If ($PercentFree -gt 0.1) {
            $Status = "Green"
        } ElseIf ($PercentFree -gt 0.05) {
            $Status = "Yellow"
        } Else {
            $Status = "Red"
        }
        @{
            PercentFree = $PercentFree.toString("P")
            Status      = $Status
        } | ConvertTo-Json
    }

    Get '/health/disk/{letter}' {
        $Disk = Get-CimInstance Win32_LogicalDisk -Filter "DeviceID='$($parameters.letter):'"
        $PercentFree = $Disk.FreeSpace / $Disk.Size
        If ($PercentFree -gt 0.25) {
            $Status = "Green"
        } ElseIf ($PercentFree -gt 0.1) {
            $Status = "Yellow"
        } Else {
            $Status = "Red"
        }
        @{
            PercentFree = $PercentFree.toString("P")
            Status      = $Status
        } | ConvertTo-Json
    }

    Get '/health/cpu' {
        $Usage = Get-CimInstance -ClassName win32_processor `
        | Measure-Object -Property LoadPercentage -Average `
        | Select-Object -ExpandProperty Average
        If ($Usage -lt 80) {
            $Status = "Green"
        } ElseIf ($Usage -lt 95) {
            $Status = "Yellow"
        } Else {
            $Status = "Red"
        }
        @{
            CpuLoadPercentage = ($Usage / 100).toString("P")
            Status            = $Status
        } | ConvertTo-Json
    }
)