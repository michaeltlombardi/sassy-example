# PSake makes variables declared here available in other scriptblocks
# Init some things
Properties {
    # Find the build folder based on build system
    $ProjectRoot   = $PSScriptRoot
    $BuildFolder   = "$ProjectRoot/out"
    $BuiltScript   = "$ProjectRoot/out/sassy.ps1"
    $Binary        = "$ProjectRoot/out/sassy.exe"
    $Timestamp     = Get-date -uformat "%Y%m%d-%H%M%S"
    $PSVersion     = $PSVersionTable.PSVersion.Major
    $HelpOutput    = "Results_Help`_$TimeStamp.xml"
    $SpecOutput    = "Results_Specs_PS$PSVersion`_$TimeStamp.xml"
    [string]$Url = "http://localhost:8001"
    [string]$Name = "sassy"
    [string]$DisplayName = 'Sassy API'
    [string]$Description = 'API for checking health and info from a node'
    [version]$Version = '0.1.0'
}

Task Default -Depends Compile

Task Init {
    If (Test-Path $BuildFolder) {
        Remove-Item -Path "$BuildFolder/*"
    } Else {
        New-Item -Path $BuildFolder -ItemType Directory
    }
}

Task Compile -Depends Init {
    $RawContent = Get-Content $ProjectRoot/Source/sassy.ps1
    $UpdatedContent = $RawContent.Replace('<FLANCY_URL>',$Url)
    $UpdatedContent | Out-File $BuiltScript
}

Task Build  -Depends Compile {
    $BinaryBuildOptions = @{ 
        Path        = $BuiltScript 
        Destination = $Binary 
        Name        = $Name 
        DisplayName = $DisplayName 
        Description = $Description 
        Version     = $Version
        
    }
    New-ScriptAsService @BinaryBuildOptions
}

Task Install {
    If (Test-Path $Binary) {
        $InstallOptions = @{
            Path        = $Binary
            Name        = $Name 
            Description = $Description 
        }
        Install-ScriptAsService @InstallOptions
    } Else {
        Write-Warning "$Binary"
        Write-Warning "No binary found in the output folder; ensure that it is built before trying to install!"
    }
}

Task Uninstall {
    If (Get-Service $Name) {
        Write-Host "Stopping the $Name service" -ForegroundColor DarkMagenta
        Stop-Service -Name $Name
        Write-Host "Uninstalling $Name service" -ForegroundColor DarkMagenta
        Uninstall-ScriptAsService -Name $Name
    }
}